import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
const baseUrl = 'https://api.schiphol.nl/public-flights/flights?app_id=98faca3a&app_key=1e33d1da576fdd26fa1348480459aa73';
@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css']
})
export class FlightsComponent implements OnInit {
  flights: any;
  departureDay: string;
  departureTime: string;
  constructor(private http: HttpClient) { 
  }

  getFlights(day,time) {
  
    this.http.get(baseUrl+'&scheduledate=' + day + '&scheduletime='+ time + '&flightdirection=D&route=OTP%2CLED%2CMEX&includedelays=false&page=0&sort=%2Bscheduletime', {
      headers: {
        "ResourceVersion" : "v3"
      }
    }).subscribe((data:any) => {
      console.log(data.flights);
      let flight = data.flights[0];
      console.log(flight);
      this.flights = data.flights;
    })
  }

  ngOnInit() {
    this.departureDay = '2018-07-03';
    this.departureTime = '16:30';
  }

  onSubmit() {
    this.getFlights(this.departureDay, this.departureTime);
  }

}


