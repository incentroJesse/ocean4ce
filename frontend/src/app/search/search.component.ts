import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {

 
  baseUrl = 'https://www.rijksmuseum.nl/api/nl/collection/';
  artObjects = [];
  artObjectsCount: number;
  objectNumber: string;
  randomNumber: number;
  artObject = [];
  artImage: string;
  artTitle: string;

  constructor(private http: HttpClient) {}

  getArtObjectDetails(objectNumber: string) {
  	return this.http.get(this.baseUrl + objectNumber + '?key=J1NoRRhc&format=json');
  }

  getArtObjects() {
  	this.http.get('https://www.rijksmuseum.nl/api/nl/collection?key=J1NoRRhc&format=json').subscribe((data: any) => {
      //Get random number for object
      let randomNumber = Math.floor(Math.random() * 10);
      console.log(randomNumber);
      const artObject = data.artObjects[randomNumber];
      console.log(artObject);
      this.artTitle = artObject.title;
      this.artImage = artObject.webImage.url; 

      // this.getArtObjectDetails(object.objectNumber).subscribe((data: any) => {
      //   // fetch the details
      //   artObject.dimensions = data.artObject.dimensions;
      // });

      // this.artObjects.push(artObject); // push to array
  		
  	});
  }
}



