import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessPercentageComponent } from './success-percentage.component';

describe('SuccessPercentageComponent', () => {
  let component: SuccessPercentageComponent;
  let fixture: ComponentFixture<SuccessPercentageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessPercentageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessPercentageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
