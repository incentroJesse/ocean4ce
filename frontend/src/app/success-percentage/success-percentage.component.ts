import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-success-percentage',
  templateUrl: './success-percentage.component.html',
  styleUrls: ['./success-percentage.component.css']
})
export class SuccessPercentageComponent implements OnInit {

  successPercentage: any;

  constructor() { }

  ngOnInit() {
    this.successPercentage = Math.floor(Math.random() * 100);
  }



}
