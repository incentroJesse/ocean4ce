import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vault',
  templateUrl: './vault.component.html',
  styleUrls: ['./vault.component.css']
})
export class VaultComponent implements OnInit {
  code: number;
  luckyNumber: any;
  codeString: string;
  succesPercentage: number;
  succesString: string;

  constructor() { }

  ngOnInit() {
  }

  calculate() {
    if (this.luckyNumber !== undefined && !this.luckyNumber.toString().isEmpty) {
    console.log('CALCULATING VAULT PASSCODE');
      do {
        this.code = Math.floor(Math.random() * (10000));
      }
      while (!this.code.toString().includes(this.luckyNumber.toString()));

      if (this.code < 1000) {
        this.codeString = '0' + this.code.toString();
      } else {
        this.codeString = this.code.toString();
      }

    this.succesPercentage = Math.floor(Math.random() * 100);
      this.succesString = this.succesPercentage.toString() + '%';

    console.log('PASSCODE CALCULATED! RESULT: ' + this.codeString);
    }
  }

}
